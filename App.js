import React from 'react';
import RootStack from './app/navigation';
import store from './app/architecture/test';

import { Provider } from 'react-redux';

export default App = () => (

  <Provider store={ store }>
    <RootStack />
  </Provider>
)
