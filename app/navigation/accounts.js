
import { menuIcon } from '../common/ui/menuBar';
import styles, { 
    menuNavigationOptions, 
    menuDrawerNavigationOptions, 
    defaultTabOptions } from '../common/styles';

import { 
    createStackNavigator, 
    createMaterialTopTabNavigator } from 'react-navigation';

import CompanyScreen from '../usecases/accounts/company';
import TravelAgentScreen from '../usecases/accounts/travelAgent';
import DetailsScreen from '../usecases/accounts/details';
import TasksScreen from '../usecases/accounts/tasks';
import AddTaskScreen from '../usecases/accounts/tasks/add';
import FilterScreen from '../usecases/accounts/filters';


const AccountsTab = createMaterialTopTabNavigator(
    {
        company: CompanyScreen,
       'travel Agent': TravelAgentScreen
    }, {
        tabBarOptions: defaultTabOptions()
    }
)

const DetailsTab = createMaterialTopTabNavigator(
    {
        details: DetailsScreen,
        tasks: TasksScreen
    }, {
        tabBarOptions: defaultTabOptions()
    }
)


export const AccountsStack = createStackNavigator(
    {
        accounts: {
            screen: AccountsTab,
            navigationOptions: ({ navigation }) => menuDrawerNavigationOptions(navigation, 'ACCOUNTS')
        },
        showDetails: {
            screen: DetailsTab,
            navigationOptions : {
                title: 'Account Details',
            }
        },
        newTask: {
            screen: AddTaskScreen,
            navigationOptions: {
                title: 'Add Task'
            },
        },
        filters: {
            screen: FilterScreen,
            navigationOptions: {
                title: 'Filters'
            }
        }
    }, 
    {
        navigationOptions: menuNavigationOptions(),
        mode: 'card'
    }
)
