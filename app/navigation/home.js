import { 
    createStackNavigator, 
    createDrawerNavigator } from 'react-navigation';

import { menuNavigationOptions, menuDrawerNavigationOptions } from '../common/styles';


import { AccountsStack } from './accounts';
import SearchScreen from '../usecases/search';
import BookingLeadsScreen from '../usecases/bookingLeads';


const SearchStack = createStackNavigator(
    {
        search: SearchScreen
    }, {
        navigationOptions: ({ navigation }) => menuDrawerNavigationOptions(navigation, 'Search')

    }
)

const BookingLeadsStack = createStackNavigator(
    {
        bookingLeads: BookingLeadsScreen
    }, {
        navigationOptions: ({ navigation }) => menuDrawerNavigationOptions( navigation , 'Booking Leads')
    }
)

export const HomeDrawer = createDrawerNavigator(
    {
        search : SearchStack,
        accounts : AccountsStack,
        'booking leads' : BookingLeadsStack
    }, {
        initialRouteName: 'search', 
    }
)