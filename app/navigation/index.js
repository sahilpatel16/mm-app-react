import SplashScreen from '../usecases/splash';
import LoginScreen from '../usecases/login';
import HomeScreen from '../usecases/home';

import { createStackNavigator } from 'react-navigation';
import { 
    SplashStack,
    LoginStack,
    HomeStack } from './everythingElse';

import { HomeDrawer } from './home';

navigationOptions = {
    headerStyle: {
        backgroundColor: '#f4511e'
    },
    mode: 'card',
    headerTintColor: '#fff',
    headerTitleStyle: {
        fontWeight: 'bold',
    },
}

const RootStack = createStackNavigator(
    {
        splashStack: SplashStack,
        loginStack: LoginStack,
        home: HomeDrawer
    }, {
        initialRouteName: 'splashStack',
        navigationOptions: {
            header: null
        }
    }
)

export default RootStack