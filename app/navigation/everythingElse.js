import { createStackNavigator } from 'react-navigation';

import SplashScreen from '../usecases/splash';
import LoginScreen from '../usecases/login';
import HomeScreen from '../usecases/home';


export const SplashStack = createStackNavigator (
    {
        splash : SplashScreen
    }, {
        initialRouteKey: 'splash'
    }
)

export const LoginStack = createStackNavigator (
    {
        login : LoginScreen
    }, {
        initialRouteKey: 'login',
        navigationOptions: {
            headerStyle: {
                backgroundColor: '#f4511e'
            },
            mode: 'card',
            headerTintColor: '#fff',
            headerTitleStyle: {
                fontWeight: 'bold',
            },
        }
    }
)

export const HomeStack = createStackNavigator (
    {
        home: HomeScreen
    }, {
        initialRouteKey: 'home'
    }
)





