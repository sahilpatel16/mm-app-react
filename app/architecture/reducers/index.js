import { combineReducers } from 'redux';
import appStartup from '../../usecases/splash/states/reducers';
import home from '../../usecases/home/states/reducers';
import login from '../../usecases/login/states/reducers';

const rootReducer = combineReducers({
    appStartup,
    login,
    home
})

export default rootReducer

