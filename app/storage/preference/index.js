import { AsyncStorage } from 'react-native';


export default class Preference {

    static save = async (key, value) => {
        try {
            await AsyncStorage.setItem(key, value)
        } catch (error) {
            console.log(error)
        }
    }

    static saveAll = async (pairs = []) => {
        console.log(pairs)
        try {
            items = pairs.map( pair =>{
                // console.log(pair.key+" "+pair.value)
                return AsyncStorage.setItem(pair.key, pair.value)
            })
            await Promise.all(items)
        } catch (error) {
            console.log(error)
        }
    }

    static retrieve = async (key, defaultValue) => {
        try {

            const value = await AsyncStorage.getItem(key)
            if (value !== null) {
                console.log(value)
                return value
            }
            else {
                return defaultValue
            }
        } catch (error) {
            console.log(error)
        }
    }

    static clearAll = async () => {
        try {
            await AsyncStorage.clear()
        } catch (error) {
            console.log(error)
        }
    }
}

// export const save = async (key, value) => {
//     try {
//         await AsyncStorage.setItem(key, value)
//     } catch (error) {
//         console.log(error)
//     }
// }

// export const retrieve = async (key, defaultValue) => {
//     try {

//         const value = await AsyncStorage.getItem(key)
//         if (value !== null) {
//             console.log(value)
//             return value
//         } 
//         else {
//             return defaultValue
//         }
//     } catch (error) {
//         console.log(error)  
//     }
// }

