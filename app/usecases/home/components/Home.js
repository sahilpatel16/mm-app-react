import React from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import styles from '../../../common/styles';

import { getUserDataFromPreferences } from '../utils';


export default class Home extends React.Component {

    componentDidMount = () => {
        getUserDataFromPreferences()
        .then( name => this.props.updateUserName(name))
        .catch( error => console.log('Some error : '+error))
    }

    componentDidUpdate(prevProps) {
        if(!prevProps.hasLoggedOut && this.props.hasLoggedOut ) {
            this.props.navigation.navigate('login')
        }
    }

    render = () => (
        <View style={styles.container}>
            <Text style={{ 
                marginBottom: 20,
             }}>Hello {this.props.userName}</Text>
            <Button 
                title={'Logout'}
                onPress = { () => this.props.requestLogout() }
            />
        </View>
    )
}