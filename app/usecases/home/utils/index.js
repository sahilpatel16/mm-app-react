import Preference from '../../../storage/preference';
import { PREF_KEY_FIRST_NAME } from '../../../common/constants';

export const getUserDataFromPreferences = () => {
    
    return Preference.retrieve(PREF_KEY_FIRST_NAME, '') 
    .then(name => {
        if (name === '') {
            throw Error('No user name found.')
        } else {
            return name
        }
    })
}

export const logoutUser = () => {
    return Preference.clearAll()
}