export const UPDATE_USER_NAME = "UPDATE_USER_NAME"
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS"
export const LOGOUT_FAILED = "LOGOUT_FAILED"

import { logoutUser } from '../utils';

export const updateUserName = userName => ({
    type: UPDATE_USER_NAME,
    userName
})

const processLogoutRequest = hasSucceeded => ({
    type: hasSucceeded ? 
        LOGOUT_SUCCESS :
        LOGOUT_FAILED 
})

export const requestLogout = () => {
    
    return dispatch => {
        logoutUser()
        .then(() => dispatch(processLogoutRequest(true)))
        .catch(error => {
            console.log(error)
            dispatch(processLogoutRequest(false))
        })
    }
}