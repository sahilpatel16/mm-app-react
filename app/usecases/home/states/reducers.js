import { 
    UPDATE_USER_NAME,
    LOGOUT_SUCCESS,
    LOGOUT_FAILED
 } from './actions';

const home = (state = {}, action) => {
    switch(action.type) {
        case UPDATE_USER_NAME:
            return Object.assign({}, state, {
                userName: action.userName
            })
            break;
        
        case LOGOUT_SUCCESS:
            return Object.assign({}, state, {
                hasLoggedOut: true
            })
            break;

        case LOGOUT_FAILED:
            return Object.assign({}, state, {
                hasLoggedOut: false
            })
            break;
        
        default:
            return state
    }
}

export default home