import React from 'react';
import { connect } from 'react-redux';

import { updateUserName, requestLogout } from './states/actions';
import { Text, View, StyleSheet } from 'react-native';
import Home from './components/Home';


class HomeScreen extends React.Component {

    static navigationOptions = {
        title: 'Home',
        headerLeft: null
    }

    render = () => (
        <Home {...this.props}/>
    )
}

const mapStateToProps = state => ({
    userName : state.home.userName,
    hasLoggedOut : state.home.hasLoggedOut
})

const mapDispatchToProps = dispatch => ({
    updateUserName : userName => dispatch(updateUserName(userName)),
    requestLogout : () => dispatch(requestLogout())
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen)


