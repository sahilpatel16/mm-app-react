import React from 'react';
import { Text, View, Button } from 'react-native';

export default class SearchScreen extends React.Component {

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Search</Text>
                <Button
                    title="Next"
                    onPress={() => this.props.navigation.navigate('login')}
                />
            </View>
        )
    }
}