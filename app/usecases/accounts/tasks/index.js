import React from 'react';
import { Text, View, Button } from 'react-native';

export default class TasksTabScreen extends React.Component {

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Tasks Tab</Text>
                <Button
                    title="AddTask"
                    onPress={() => this.props.navigation.navigate('newTask')}
                />
            </View>
        )
    }
}