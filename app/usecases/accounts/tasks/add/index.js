import React from 'react';
import { Text, View, Button } from 'react-native';

export default class AddTaskModalScreen extends React.Component {

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Add Task</Text>
                <Button
                    title="Save"
                    onPress={() => this.props.navigation.navigate('home')}
                />
            </View>
        )
    }
}