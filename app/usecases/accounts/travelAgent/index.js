import React from 'react';
import { Text, View, Button } from 'react-native';

export default class TravelAgentScreen extends React.Component {

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Travel Agent Tab</Text>
                <Button
                    title="Filters"
                    onPress={() => this.props.navigation.navigate('filters')}
                />
            </View>
        )
    }
}