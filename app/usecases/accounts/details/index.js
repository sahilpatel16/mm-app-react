import React from 'react';
import { Text, View, Button } from 'react-native';

export default class DetailsTabScreen extends React.Component {

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Details Tab</Text>
                <Button
                    title="Show Details"
                    onPress={() => this.props.navigation.navigate('showDetails')}
                />
            </View>
        )
    }
}