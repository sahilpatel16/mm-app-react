import React from 'react';
import { Text, View, Button } from 'react-native';

export default class TravelAgentScreen extends React.Component {

    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Company Tab</Text>
                <Button
                    title="Show Details"
                    onPress={() => this.props.navigation.navigate('showDetails')}
                />
            </View>
        )
    }
}