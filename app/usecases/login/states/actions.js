export const TOGGLE_LOADING = 'TOGGLE_LOADING'
export const RECEIVE_ERROR = 'RECEIVER_ERROR'
export const UPDATE_FIELD = 'UPDATED_FIELD'
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS'
export const LOGIN_FAILURE = 'LOGIN_FAILURE'

import { 
    makeLoginRequestWith, 
    handleLoginResponse,
    enableAutoLogin } from '../utils';

export const toggleLoadingScreenVisibility = (isLoading, message = '') => ({
    type: TOGGLE_LOADING,
    isLoading,
    message
})

export const updateLoginStatus = hasSucceeded => ({
    type : hasSucceeded ?
        LOGIN_SUCCESS : 
        LOGIN_FAILURE
})

export const processErrorResponse = (errorTitle = '', errorMessage = '', errorCode = '666') => ({
    type : RECEIVE_ERROR, 
    errorTitle,
    errorMessage,
    errorCode
})

export const updateInputField = (fieldName, value) => ({
    type : UPDATE_FIELD,
    fieldName,
    value
})


export const requestLogin = (email, password) => {
    return dispatch => {

        //  Enabling the loading screen
        dispatch(toggleLoadingScreenVisibility(true, 'Logging you in !'))
        
        //  making the api call
        makeLoginRequestWith(email, password)
        .then(json => {

            //  Disabling the loading screen.
            dispatch(toggleLoadingScreenVisibility(false))

            //  if json has error object. We call it a failed login
            if (json.error) {
                
                //  An action to broadcast that login has failed.
                dispatch(updateLoginStatus(false))
                dispatch(processErrorResponse(json.error.type, json.error.message, json.error.code))
            } else {
              
                //  Saving everything locally.
                handleLoginResponse(json.data)
                .then(() => enableAutoLogin())
                .then(() => dispatch(updateLoginStatus(true)))
            }
        })
    }
}