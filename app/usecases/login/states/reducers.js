import {
    TOGGLE_LOADING,
    RECEIVE_ERROR,
    UPDATE_FIELD,
    LOGIN_SUCCESS,
    LOGIN_FAILURE
} from './actions';

const initialState = {
    fields : []
}

const login = (state = initialState, action) => {

    switch (action.type) {
        case TOGGLE_LOADING:
            return Object.assign({}, state, {
                isLoading: action.isLoading,
                loadingMessage: action.message
            })
            break;
        
        case RECEIVE_ERROR:
            return Object.assign({}, state, {
                errorCode: action.errorCode,
                errorTitle: action.errorTitle,
                errorMessage: action.errorMessage
            })
            break;
        
        case UPDATE_FIELD:
            return Object.assign({}, state, {
                fields: {
                    ...state.fields,
                    [action.fieldName]: action.value
                }
            })
        
        case LOGIN_SUCCESS:
            return Object.assign({}, state, {
                loginSuccess: true
            })
    
        case LOGIN_FAILURE:
            return Object.assign({}, state, {
                loginSuccess: false
            })

        default:
            return state;
    }
}

export default login