import React from 'react';
import { Text, 
    View, 
    StyleSheet,
    ActivityIndicator } from 'react-native';
import { translucentBackgroundColor, loadingIndicatorColor } from '../../../common/colours';

export default class Loading extends React.Component {

    render() {

        if (this.props.hide) {
            return null
        }
        else {
        return (
            <View 
                height={'100%'}
                style={ styles.container }>
                <View 
                    style={ styles.centerContainer }>
                    <ActivityIndicator 
                        animating={ this.props.showing } 
                        size="large" color={ loadingIndicatorColor } />

                    <Text
                        style={ styles.loadingText }>
                        { this.props.text == undefined ? 'Please wait' : this.props.text }
                    </Text>
                </View>

            </View>
        )
    }
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor: translucentBackgroundColor,
    },
    centerContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    loadingText: {
        marginTop: 20,
        color: 'white',
        fontSize: 16
    }  
})