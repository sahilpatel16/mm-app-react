import React from 'react';
import { 
    Text, 
    Alert, 
    View, 
    KeyboardAvoidingView, 
    TextInput, 
    StyleSheet, 
    Button, 
    Keyboard } from 'react-native';

import { buttonGreenColor } from '../../../common/colours';

class Login extends React.Component {

    showAlert() {
        Alert.alert(
            'Alert Title',
            'My Alert Msg',
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    render() {
        return (
            <KeyboardAvoidingView 
                style={ styles.container }
                behavior="padding"
            >
                <View 
                    width={'80%'} >
                    <TextInput 
                        onChangeText={(text) => this.props.updateFields('email', text)}
                        value= { this.props.fields['email'] }
                        keyboardType={'email-address'}
                        style={ styles.inputField }
                        placeholder={'Email'}
                    />

                    <TextInput
                        onChangeText={(text) => this.props.updateFields('password', text)}
                        value={this.props.fields['password']}
                        blurOnSubmit={true}
                        secureTextEntry={true}
                        style={[styles.inputField, { marginTop: 20 }]}
                        placeholder={'Password'}
                    />

                    <View 
                        style={{ backgroundColor: 'yellow' }}
                        width={'40%'}                        
                        style={{ marginTop: 15, }}>
                        <Button
                            disabled = { !this.props.fields['email'] || !this.props.fields['password'] }
                            color={ buttonGreenColor }
                            onPress={ () => { 
                                Keyboard.dismiss()
                                this.props.requestLogin(this.props.fields['email'], this.props.fields['password']) } }
                            title="Login"
                        />
                    </View>                    
                </View>
            </KeyboardAvoidingView>
        ) 
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputField: {
        paddingLeft: 5,
        paddingBottom: 10,
        fontSize: 16,
        height: 40,
    }
});

export default Login