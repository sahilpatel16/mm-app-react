import { postData } from '../../../storage/api';
import Preference from '../../../storage/preference';

export const makeLoginRequestWith = (email, password) => {

    const url = 'http://mm-web-server.ap-southeast-1.elasticbeanstalk.com/mm/api/crs_v1/users/sign_in'
    return postData(url, {
        email: email,
        password: password
    })
        .then(response => response.json())
        .catch(error => console.log("Error occurred : " + error))
}


export const handleLoginResponse = (data) => {

    items = [
        {
            key: 'firstName',
            value: data.attributes.first_name
        },
        {
            key: 'email',
            value: data.attributes.email
        },
        {
            key: 'microMarketId',
            value: (data.attributes.micro_market.id).toString()
        },
        {
            key: 'authToken',
            value: data.attributes.current_authentication_token
        },
        {
            key: 'userProfileId',
            value: data.relationships.user_profile.data.id
        },
        {
            key: 'role',
            value: data.relationships.access_profile.data.access_category
        },
        {
            key: 'user',
            value: JSON.stringify(data)
        }
    ]

    console.log(items)
    return Preference.saveAll(items)
}

export const enableAutoLogin = () => {
    return Preference.save('isLoggedIn', 'true')
}