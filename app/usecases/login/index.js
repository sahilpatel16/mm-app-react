import React from 'react';
import { View, Alert } from 'react-native';
import { connect } from 'react-redux';

import Loading from './components/Loading';
import LoginScreen from './components/Login';

import { 
    processErrorResponse, 
    updateInputField, 
    requestLogin } from './states/actions';

class Login extends React.Component {

    static navigationOptions = {
        headerLeft: null,
        title: 'Login'
    }

    //  Shows an alert message for the error
    showAlert(title, message, onOkPress) {
        Alert.alert( title, message,
            [{ 
                text: 'OK', 
                onPress: onOkPress 
            }],
            { cancelable: false }
        )
    }

    componentDidUpdate(prevProps) {

        //  If a new error message is added, we show it as dialog
        if (!prevProps.errorMessage && this.props.errorMessage) {
            this.showAlert(this.props.errorTitle, this.props.errorMessage, () => {
                this.props.resetErrorFlag()
            })
        }

        //  If login success flag is set to true recently, we move to home screen.
        if (!prevProps.loginSuccess && this.props.loginSuccess) {
            this.props.navigation.navigate('home')
        }
    }

    render = () => (
        <View style={{ flex: 1, }}>
            <Loading hide={!this.props.isLoading} text={this.props.loadingMessage} />
            <LoginScreen
                fields={this.props.fields}
                updateFields={this.props.updateInputField}
                requestLogin={this.props.requestLogin} />
        </View>
    )
}

const mapStateToProps = state => ({
    isLoading: state.login.isLoading,
    loadingMessage: state.login.loadingMessage,
    errorMessage: state.login.errorMessage,
    errorTitle: state.login.errorTitle,
    fields: state.login.fields,
    loginSuccess: state.login.loginSuccess
})

const mapDispatchToProps = dispatch => ({
    requestLogin: (email, password) => dispatch(requestLogin(email, password)),
    resetErrorFlag: () => dispatch(processErrorResponse(undefined, undefined)),
    updateInputField: (field, value) => dispatch(updateInputField(field, value))
})

export default connect (
    mapStateToProps,
    mapDispatchToProps )(Login)


