import React from 'react';
import { 
    Text, 
    View, 
    StyleSheet } from 'react-native';

import { SPLASH_SCREEN_TIMEOUT } from '../../../common/constants';
import Utils from '../utils';
import styles from '../../../common/styles';

export default class Splash extends React.Component {

    componentDidMount() {
        // The app is now ready to perform actions
        setTimeout(() => this.props.toggleAppReady(true), SPLASH_SCREEN_TIMEOUT)
    }

    componentDidUpdate(prevProps) {

        //  App is ready to handle state changes
        if (this.props.isAppReady != undefined && this.props.isAppReady) {

            //  user is not already logged in
            if (this.props.isLoggedIn == undefined) {

                //  Getting logged in flag from local preferences.
                Utils.getUserStateFromPreferences()
                .then(hasLoggedIn => this.props.toggleLoggedIn(hasLoggedIn))
                .catch( (error) => { 
                    console.log('Error while reading logged in boolean : '+error)
                    this.props.toggleLoggedIn(false)
                })
            }
            else {
                //  opening either login screen or home screen based on the 'isLoggedIn' flag
                this.props.navigateTo(this.props.isLoggedIn, this.props.navigation)
            }
        }
    }

    render = () => (
        <View style={styles.container}>
            <Text style={splashStyles.textStyle}>OYO</Text>
            <Text> App Ready : {
                this.props.isAppReady != undefined && this.props.isAppReady ?
                    'true' :
                    'false'
            } </Text>
        </View>
    )
}

const splashStyles = StyleSheet.create({
    textStyle: {
        fontSize: 30,
        fontWeight: '300',
    }
})