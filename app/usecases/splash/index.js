import React from 'react';
import { connect } from 'react-redux';

import { toggleAppReady, toggleLoggedIn } from './states/actions';
import Splash from './components/Splash';

class SplashScreen extends React.Component {

    static navigationOptions = {
        header: null
    };

    render = () => (
        <Splash navigateTo={navigateTo}{...this.props} />
    )
}

const navigateTo = (loggedIn, navigation) => {
    if (!loggedIn) {
        navigation.navigate('login')
    } else {
        navigation.navigate('home')
    }
}

const mapStateToProps = state => ({
    isAppReady: state.appStartup.isAppReady,
    isLoggedIn: state.appStartup.isLoggedIn
})

const mapDispatchToProps = dispatch => ({
    toggleAppReady: appReady => dispatch(toggleAppReady(appReady)),
    toggleLoggedIn: loggedIn => dispatch(toggleLoggedIn(loggedIn))
})

export default connect(
    mapStateToProps,
    mapDispatchToProps)(SplashScreen)


// const navigateTo = (navigation) => {

//     const resetAction = StackActions.reset({
//         index: 0,
//         actions: [NavigationActions.navigate({ routeName: 'home' })],
//     });

//     navigation.dispatch(resetAction);
//     // navigation.navigate('login')
// }