import Preference from '../../../storage/preference';
import { IS_LOGGED_IN } from '../../../common/constants';

export default class SplashUtils {

    static getUserStateFromPreferences() {
        return Preference.retrieve(IS_LOGGED_IN, false)
    }
}