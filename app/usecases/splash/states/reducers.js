import { UPDATE_APP_READY, UPDATE_LOGGED_IN } from './actions';

const appStartup = (state = {}, action) => {
    switch (action.type) {
        case UPDATE_APP_READY:
            return Object.assign({}, state, {
                isAppReady : action.appReady
            })
            break;
    
        case UPDATE_LOGGED_IN:
            return Object.assign({}, state, {
                isLoggedIn: action.loggedIn
            })
            break;

        default:
            return state
    }
}

export default appStartup