export const UPDATE_LOGGED_IN = "UPDATE_LOGGED_IN"
export const UPDATE_APP_READY = "UPDATE_APP_READY"

export const toggleLoggedIn = loggedIn => ({
    type: UPDATE_LOGGED_IN,
    loggedIn
})

export const toggleAppReady = appReady => ({
    type: UPDATE_APP_READY,
    appReady
})