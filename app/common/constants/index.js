export const IS_LOGGED_IN = 'isLoggedIn'
export const IS_APP_READY = 'isAppReady'
export const SPLASH_SCREEN_TIMEOUT = 3000

export const PREF_KEY_FIRST_NAME = 'firstName'