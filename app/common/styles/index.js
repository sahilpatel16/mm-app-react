import { StyleSheet } from 'react-native';
import { defaultBackgroundColor, menuBarBackgroundColor, menuTintColor } from '../colours';
import { menuIconPadding } from '../dimensions';

import { menuIcon } from '../ui/menuBar';


export default styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: defaultBackgroundColor,
        alignItems: 'center',
        justifyContent: 'center',
    },

    menuBarStyle : {
        backgroundColor: menuBarBackgroundColor,
        paddingLeft: menuIconPadding,
    }
});

export const menuDrawerNavigationOptions = (navigation, title) => ({
    title: title,
    headerLeft: menuIcon(navigation),
    headerStyle: styles.menuBarStyle,
    headerTintColor: menuTintColor,
    headerTitleStyle: {
        fontWeight: 'bold',
        fontSize: 16,
    },
})

export const menuNavigationOptions = () => ({
    
    headerStyle: {
        backgroundColor: menuBarBackgroundColor
    },
    headerTintColor: menuTintColor,
    headerTitleStyle: {
        fontWeight: 'bold',
    },
})

export const defaultTabOptions = () => ({
    activeTintColor: 'black',
    inactiveTintColor: 'grey',
    style: {
        backgroundColor: 'white'
    },
    indicatorStyle: {
        backgroundColor: 'black'
    }
})