export const translucentBackgroundColor = 'rgba(52, 52, 52, 0.6)'
export const loadingIndicatorColor = '#0000ff'
export const buttonGreenColor = '#00a32e'
export const defaultBackgroundColor = '#EEEEEE'

export const menuTintColor = '#FFFFFF'
export const menuBarBackgroundColor = '#f4511e'
