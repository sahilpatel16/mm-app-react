import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { menuTintColor } from '../colours';

import { menuIconSize } from '../dimensions';

export const menuIcon = (navigation) => (
    <Ionicons
        name="ios-menu"
        size={ menuIconSize }
        color={ menuTintColor }
        onPress={() => { navigation.openDrawer() }}
    />
)